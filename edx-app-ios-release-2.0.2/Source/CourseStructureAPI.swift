//
//  ServerAPI.swift
//  edX
//
//  Created by Akiva Leffert on 5/20/15.
//  Copyright (c) 2015 edX. All rights reserved.
//

import UIKit

public struct CourseOutlineAPI {
    struct Parameters {
        let courseID: String
        let username : String?
        let fields : [String]
        let blockCount : [String]
        let studentViewData : [CourseBlock.Category]
        
        var query : [String:JSON] {
            var result =
            [
                "fields" : JSON(fields.joinWithSeparator(",")),
                "block_count" : JSON(blockCount.joinWithSeparator(",")),
                "block_json" : "{\"video\":{\"profiles\":[\"mobile_high\",\"mobile_low\"]}}",
                "depth": "all",
                "navigation_depth": 3,
                "course_id": JSON(courseID)
                
//                "requested_fields" : JSON(fields.joinWithSeparator(",")),
//                "block_counts" : JSON(blockCount.joinWithSeparator(",")),
//                "student_view_data" : JSON(studentViewData.map({ $0.rawValue }).joinWithSeparator(",")),
//                "depth": "all",
//                "nav_depth": 3,
//                "course_id": JSON(courseID)
            ]
            
            if let username = username {
                result["username"] = JSON(username)
            }
            
            return result
            
        }
    }
    
    static func deserializer(response : NSHTTPURLResponse, json : JSON) -> Result<CourseOutline> {
        return CourseOutline(json: json).toResult(NSError.oex_courseContentLoadError())
    }
    
    public static func requestWithCourseID(courseID : String, username : String?) -> NetworkRequest<CourseOutline> {
        print("EM DUOC GOI NHUNG KHONG CHAY")
        let parameters = Parameters(
            courseID: courseID,
            username: username,
            fields : ["graded", "multi_device", "format"],
            blockCount : [CourseBlock.Category.Video.rawValue],
            studentViewData : [CourseBlock.Category.Video]
            
        )
        print(parameters.query)
        return NetworkRequest(
            method : HTTPMethod.GET,
            
            //path : "/api/courses/v1/blocks/",
            //path : "/api/course_structure/v0/courses/course-v1:FUNiX+FUN111x+2015_T11/blocks/",
            path : "/api/course_structure/v0/courses/{course_id}/blocks+navigation/".oex_formatWithParameters(["course_id" : courseID]),
            requiresAuth : true,
            query : parameters.query,
            deserializer : .JSONResponse(deserializer)
        )
    }
    
    

}
