//
//  OEXHandoutsViewController.h
//  edXVideoLocker
//
//  Created by Abhradeep on 17/02/15.
//  Copyright (c) 2015 edX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OEXHandoutsViewController : UIViewController

- (instancetype)initWithHandoutsString:(NSString*)handoutsString;

@end
